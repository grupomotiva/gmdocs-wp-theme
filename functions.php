<?php
/**
* Set the content width based on the theme's design and stylesheet.
*
* @since GMDOCS 1.0
*/
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
* GMDOCS only works in WordPress 4.1 or later.
*/
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}


/**
* Automatically add IDs to headings such as <h2></h2>
*/
function auto_id_headings( $content ) {
	$content = preg_replace_callback( '/(\<h[1-6](.*?))\>(.*)(<\/h[1-6]>)/i', function( $matches ) {
		if ( ! stripos( $matches[0], 'id=' ) ) :
			$heading_link = '<a href="#' . strtolower( sanitize_title( $matches[3] )) . '" class="heading-link"><i class="glyphicon glyphicon-link"></i></a>';
			$matches[0] = $matches[1] . $matches[2] . ' id="' . strtolower( sanitize_title( $matches[3] )) . '">' . $heading_link . $matches[3] . $matches[4];
		endif;
		return $matches[0];
	}, $content );
	return $content;
}
add_filter( 'the_content', 'auto_id_headings' );



function gmdocs_list_child_pages() {
	global $post;
	$output = '';

	if ( is_home([$post->ID]) || is_front_page([$post->ID]) ) {
		// query pages whitout parent
		$args = array(
			'post_parent' => 0,
			'post_type' => 'page',
			'post__not_in' => [$post->ID],
		);
	} else {
		//query subpages
		$args = array(
			'post_parent' => $post->ID,
			'post_type' => 'page',
			'order' => 'asc'
			);
		}

		// print_r($args);
		$subpages = new WP_query($args);


		// create output
		if ($subpages->have_posts()) :

		$output;
		while ($subpages->have_posts()) : $subpages->the_post();
		$output .= '<div class="item item-green col-md-4 col-sm-6 col-xs-6">
			<div class="item-inner">
				<div class="icon-holder">
					<i class="icon fa fa-paper-plane"></i>
				</div>
				<h3 class="title">
					<a href="'.get_permalink().'">'.get_the_title().'</a>
				</h3>
			</div>
		</div>';
		// <p class="intro">'.get_the_excerpt().'</p>
		endwhile;
		$output .= '';
		else :
		$output = '<div class="item item-red col-md-12 col-sm-12 col-xs-12 text-center"></div>';
		endif;

		// reset the query
		wp_reset_postdata();

		// return something
		return $output;

	}
	add_shortcode('gmdocs-childpages', 'gmdocs_list_child_pages');



	if ( ! function_exists( 'gmdocs_setup' ) ) :
	/**
	* Sets up theme defaults and registers support for various WordPress features.
	*
	* Note that this function is hooked into the after_setup_theme hook, which
	* runs before the init hook. The init hook is too late for some features, such
	* as indicating support for post thumbnails.
	*
	* @since GMDOCS 1.0
	*/
	function gmdocs_setup() {

		load_theme_textdomain( 'gmdocs' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support( 'title-tag' );

		/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		*/
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 825, 510, true );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'gmdocs' ),
		'social'  => __( 'Social Links Menu', 'gmdocs' ),
		) );

		/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
		add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		) );

		/*
		* Enable support for Post Formats.
		*
		* See: https://codex.wordpress.org/Post_Formats
		*/
		add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
		) );

		/*
		* Enable support for custom logo.
		*
		* @since GMDOCS 1.5
		*/
		add_theme_support( 'custom-logo', array(
		'height'      => 248,
		'width'       => 248,
		'flex-height' => true,
		) );

		// Setup the WordPress core custom background feature.

		/**
		* Filter GMDOCS custom-header support arguments.
		*
		* @since GMDOCS 1.0
		*
		* @param array $args {
		*     An array of custom-header support arguments.
		*
		*     @type string $default-color     		Default color of the header.
		*     @type string $default-attachment     Default attachment of the header.
		* }
		*/
		// add_theme_support( 'custom-background', apply_filters( 'gmdocs_custom_background_args', array(
		// 	'default-color'      => $default_color,
		// 	'default-attachment' => 'fixed',
		// ) ) );

		/*
		* This theme styles the visual editor to resemble the theme style,
		* specifically font, colors, icons, and column width.
		*/
		add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', gmdocs_fonts_url() ) );

		// Indicate widget sidebars can use selective refresh in the Customizer.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
	endif; // gmdocs_setup
	add_action( 'after_setup_theme', 'gmdocs_setup' );

	/**
	* Register widget area.
	*
	* @since GMDOCS 1.0
	*
	* @link https://codex.wordpress.org/Function_Reference/register_sidebar
	*/
	function gmdocs_widgets_init() {
		register_sidebar( array(
		'name'          => __( 'Widget Area', 'gmdocs' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'gmdocs' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
				) );
			}
			add_action( 'widgets_init', 'gmdocs_widgets_init' );

			if ( ! function_exists( 'gmdocs_fonts_url' ) ) :
			/**
			* Register Google fonts for GMDOCS.
			*
			* @since GMDOCS 1.0
			*
			* @return string Google fonts URL for the theme.
			*/
			function gmdocs_fonts_url() {
				$fonts_url = '';
				$fonts     = array();
				$subsets   = 'latin,latin-ext';

				/*
				* Translators: If there are characters in your language that are not supported
				* by Noto Sans, translate this to 'off'. Do not translate into your own language.
				*/
				if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'gmdocs' ) ) {
					$fonts[] = 'Noto Sans:400italic,700italic,400,700';
				}

				/*
				* Translators: If there are characters in your language that are not supported
				* by Noto Serif, translate this to 'off'. Do not translate into your own language.
				*/
				if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'gmdocs' ) ) {
					$fonts[] = 'Noto Serif:400italic,700italic,400,700';
				}

				/*
				* Translators: If there are characters in your language that are not supported
				* by Inconsolata, translate this to 'off'. Do not translate into your own language.
				*/
				if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'gmdocs' ) ) {
					$fonts[] = 'Inconsolata:400,700';
				}

				/*
				* Translators: To add an additional character subset specific to your language,
				* translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
				*/
				$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'gmdocs' );

				if ( 'cyrillic' == $subset ) {
					$subsets .= ',cyrillic,cyrillic-ext';
				} elseif ( 'greek' == $subset ) {
					$subsets .= ',greek,greek-ext';
				} elseif ( 'devanagari' == $subset ) {
					$subsets .= ',devanagari';
				} elseif ( 'vietnamese' == $subset ) {
					$subsets .= ',vietnamese';
				}

				if ( $fonts ) {
					$fonts_url = add_query_arg( array(
					'family' => urlencode( implode( '|', $fonts ) ),
					'subset' => urlencode( $subsets ),
					), 'https://fonts.googleapis.com/css' );
				}

				return $fonts_url;
			}
			endif;

			/**
			* JavaScript Detection.
			*
			* Adds a `js` class to the root `<html>` element when JavaScript is detected.
			*
			* @since GMDOCS 1.1
			*/
			function gmdocs_javascript_detection() {
				echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
			}
			add_action( 'wp_head', 'gmdocs_javascript_detection', 0 );

			/**
			* Enqueue scripts and styles.
			*
			* @since GMDOCS 1.0
			*/
			function gmdocs_scripts() {
				wp_dequeue_script( 'jquery' );
				// Add custom fonts, used in the main stylesheet.
				wp_enqueue_style( 'gmdocs-fonts', gmdocs_fonts_url(), array(), null );

				// Add Genericons, used in the main stylesheet.
				wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/plugins/bootstrap/css/bootstrap.min.css', array(), '20141010' );
				wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/plugins/font-awesome/css/font-awesome.css', array('bootstrap'), '20141010' );

				wp_enqueue_style( 'prism', get_template_directory_uri() . '/assets/plugins/prism/prism.css', array('bootstrap'), '20141010');
				wp_enqueue_style( 'ekko-lightbox', get_template_directory_uri() . '/assets/plugins/lightbox/dist/ekko-lightbox.min.css', array('bootstrap'), '20141010');

				wp_enqueue_style( 'elegant-font', get_template_directory_uri() . '/assets/plugins/elegant_font/css/style.css', array('bootstrap'), '20141010' );
				wp_enqueue_style( 'theme-styles', get_template_directory_uri() . '/assets/css/styles.css', array('bootstrap'), '20141010' );

				// Load our main stylesheet.
				wp_enqueue_style( 'gmdocs-style', get_stylesheet_uri(), array('theme-styles'), '20141010' );


				wp_enqueue_script( 'gmdocs-jquery', get_template_directory_uri() . '/assets/plugins/jquery-1.12.3.min.js', array(), '20150330', true );
				wp_enqueue_script( 'gmdocs-bootstrap', get_template_directory_uri() . '/assets/plugins/bootstrap/js/bootstrap.min.js', array( 'gmdocs-jquery' ), '20150330', true );

				wp_enqueue_script( 'gmdocs-prism', get_template_directory_uri() . '/assets/plugins/prism/prism.js', array('gmdocs-jquery'), '20150330', true );
				wp_enqueue_script( 'gmdocs-scrollTo', get_template_directory_uri() . '/assets/plugins/jquery-scrollTo/jquery.scrollTo.min.js', array('gmdocs-jquery'), '20150330', true );
				wp_enqueue_script( 'gmdocs-lightbox', get_template_directory_uri() . '/assets/plugins/lightbox/dist/ekko-lightbox.min.js', array('gmdocs-jquery'), '20150330', true );


				wp_enqueue_script( 'gmdocs-jquery-match-height', get_template_directory_uri() . '/assets/plugins/jquery-match-height/jquery.matchHeight-min.js', array( 'gmdocs-jquery' ), '20150330', true );

				wp_enqueue_script( 'gmdocs-vue', '//cdn.jsdelivr.net/npm/vue', array( 'gmdocs-jquery' ), '20150330', true );

				wp_enqueue_script( 'gmdocs-script', get_template_directory_uri() . '/assets/js/main.js', array( 'gmdocs-vue' ), '20150330', true );


				global $post;
				$dom = new DomDocument;
				$dom->preserveWhiteSpace = FALSE;

				if ( is_home([$post->ID]) || is_front_page([$post->ID]) ) {
					// query pages whitout parent
					$args = array(
					'post_parent' => 0,
					'post_type' => 'page',
					'post__not_in' => [$post->ID],
					);
				} else {
					//query subpages
					$args = array(
					'post_parent' => $post->post_parent,
					'post_type' => 'page',
					'order' => 'asc'
					);
				}

				$currPost = $post->ID;
				$output = '';

				// print_r($args);
				$sameLevelPages = new WP_query($args);

				// create output
				if ($sameLevelPages->have_posts()) :

				while ($sameLevelPages->have_posts()) : $sameLevelPages->the_post();

				if( $currPost !== get_the_ID() ) {
					$output .= '<li>';
						$output .= '	<a href="'.get_permalink().'/">'.get_the_title().'</a>';
						$output .= '</li>';
					}else{
						$output .= '<li class="active">';
							$output .= '	<a href="'.get_permalink().'/#" class="scrolltotop">'.get_the_title().'</a>';
							$output .= '	<ul class="nav doc-sub-menu">';
								preg_match_all( '/(\<h[1-6](.*?))\>(.*)(<\/h[1-6]>)/i', get_the_content(), $menuLinks);
								foreach ($menuLinks[0] as $index => $heading) {

									$dom->loadHTML($heading);

									$h1 = $dom->getElementsByTagName('h1'); // Find Sections
									$h2 = $dom->getElementsByTagName('h2'); // Find Sections
									$h3 = $dom->getElementsByTagName('h3'); // Find Sections
									$h4 = $dom->getElementsByTagName('h4'); // Find Sections
									$h5 = $dom->getElementsByTagName('h5'); // Find Sections
									$h6 = $dom->getElementsByTagName('h6'); // Find Sections

									if($h1[0]){
										$output .= '	<li><a href="#' . strtolower( sanitize_title( $heading )) . '" class="scrollto">'.strip_tags($heading).'</a></li>';
									};

									if($h2[0]){
										$output .= '	<li class="level2"><a href="#' . strtolower( sanitize_title( $heading )) . '" class="scrollto">'.strip_tags($heading).'</a></li>';
									};

									if($h3[0]){
										$output .= '	<li class="level3"><a href="#' . strtolower( sanitize_title( $heading )) . '" class="scrollto">'.strip_tags($heading).'</a></li>';
									};

								};
								$output .= '	</ul>';
								$output .= '</li>';
							}

							endwhile;
							$output .= '';
							else :
							$output = '<div class="item item-red col-md-12 col-sm-12 col-xs-12 text-center">No docs found.</div>';
							endif;

							wp_reset_postdata();
							wp_localize_script( 'gmdocs-script', 'sidemenu', $output);

							$content = $post->post_content;
							$subMenu = [];

							preg_match_all( '/(\<h[1-2](.*?))\>(.*)(<\/h[1-2]>)/i', $content, $menuLinks);
							foreach ($menuLinks[0] as $index => $heading) {
								preg_match_all('{<h[1-2]*}', $heading, $headingTag, PREG_PATTERN_ORDER);
								$subMenu[] = '<a href="#' . strtolower( sanitize_title( $heading )) . '" class="scrollto">'.strip_tags($heading).'</a>';
							};

							wp_localize_script( 'gmdocs-script', 'submenu', $subMenu);


						}
						add_action( 'wp_enqueue_scripts', 'gmdocs_scripts' );

						/**
						* Add preconnect for Google Fonts.
						*
						* @since GMDOCS 1.7
						*
						* @param array   $urls          URLs to print for resource hints.
						* @param string  $relation_type The relation type the URLs are printed.
						* @return array URLs to print for resource hints.
						*/
						function gmdocs_resource_hints( $urls, $relation_type ) {
							if ( wp_style_is( 'gmdocs-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
								if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '>=' ) ) {
									$urls[] = array(
									'href' => 'https://fonts.gstatic.com',
									'crossorigin',
									);
								} else {
									$urls[] = 'https://fonts.gstatic.com';
								}
							}

							return $urls;
						}
						add_filter( 'wp_resource_hints', 'gmdocs_resource_hints', 10, 2 );

						/**
						* Add featured image as background image to post navigation elements.
						*
						* @since GMDOCS 1.0
						*
						* @see wp_add_inline_style()
						*/
						function gmdocs_post_nav_background() {
							if ( ! is_single() ) {
								return;
							}

							$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
							$next     = get_adjacent_post( false, '', false );
							$css      = '';

							if ( is_attachment() && 'attachment' == $previous->post_type ) {
								return;
							}

							if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
								$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
								$css .= '
								.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
								.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
								.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
								';
							}

							if ( $next && has_post_thumbnail( $next->ID ) ) {
								$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
								$css .= '
								.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); border-top: 0; }
								.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
								.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
								';
							}

							wp_add_inline_style( 'gmdocs-style', $css );
						}
						add_action( 'wp_enqueue_scripts', 'gmdocs_post_nav_background' );



						// Breadcrumbs
						function custom_breadcrumbs() {

							// Settings
							// $separator          = '/';
							$breadcrums_id      = 'breadcrumbs';
							$breadcrums_class   = 'breadcrumb';
							$home_title         = 'Home';

							// If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
							$custom_taxonomy    = 'product_cat';

							// Get the query & post information
							global $post,$wp_query;

							// Do not display on the homepage
							if ( !is_front_page() ) {

								// Build the breadcrums
								echo '<ol id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

									// Home page
									echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
									// echo '<li class="separator separator-home"> ' . $separator . ' </li>';

									if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {

										echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';

									} else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {

										// If post is a custom post type
										$post_type = get_post_type();

										// If it is a custom post type display name and link
										if($post_type != 'post') {

											$post_type_object = get_post_type_object($post_type);
											$post_type_archive = get_post_type_archive_link($post_type);

											echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
											// echo '<li class="separator"> ' . $separator . ' </li>';

										}

										$custom_tax_name = get_queried_object()->name;
										echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';

									} else if ( is_single() ) {

										// If post is a custom post type
										$post_type = get_post_type();

										// If it is a custom post type display name and link
										if($post_type != 'post') {

											$post_type_object = get_post_type_object($post_type);
											$post_type_archive = get_post_type_archive_link($post_type);

											echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
											// echo '<li class="separator"> ' . $separator . ' </li>';

										}

										// Get post category info
										$category = get_the_category();

										if(!empty($category)) {

											// Get last category post is in
											$last_category = end(array_values($category));

											// Get parent any categories and create array
											$get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
											$cat_parents = explode(',',$get_cat_parents);

											// Loop through parent categories and store in variable $cat_display
											$cat_display = '';
											foreach($cat_parents as $parents) {
												$cat_display .= '<li class="item-cat">'.$parents.'</li>';
												// $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
											}

										}

										// If it's a custom post type within a custom taxonomy
										$taxonomy_exists = taxonomy_exists($custom_taxonomy);
										if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

											$taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
											$cat_id         = $taxonomy_terms[0]->term_id;
											$cat_nicename   = $taxonomy_terms[0]->slug;
											$cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
											$cat_name       = $taxonomy_terms[0]->name;

										}

										// Check if the post is in a category
										if(!empty($last_category)) {
											echo $cat_display;
											echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

											// Else if post is in a custom taxonomy
										} else if(!empty($cat_id)) {

											echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
											// echo '<li class="separator"> ' . $separator . ' </li>';
											echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

										} else {

											echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

										}

									} else if ( is_category() ) {

										// Category page
										echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

									} else if ( is_page() ) {

										// Standard page
										if( $post->post_parent ){

											// If child page, get parents
											$anc = get_post_ancestors( $post->ID );

											// Get parents in the right order
											$anc = array_reverse($anc);

											// Parent page loop
											if ( !isset( $parents ) ) $parents = null;
											foreach ( $anc as $ancestor ) {
												$parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
												// $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
											}

											// Display parent pages
											echo $parents;

											// Current page
											echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';

										} else {

											// Just display current page if not parents
											echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';

										}

									} else if ( is_tag() ) {

										// Tag page

										// Get tag information
										$term_id        = get_query_var('tag_id');
										$taxonomy       = 'post_tag';
										$args           = 'include=' . $term_id;
										$terms          = get_terms( $taxonomy, $args );
										$get_term_id    = $terms[0]->term_id;
										$get_term_slug  = $terms[0]->slug;
										$get_term_name  = $terms[0]->name;

										// Display the tag name
										echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';

									} elseif ( is_day() ) {

										// Day archive

										// Year link
										echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
										// echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

										// Month link
										echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
										// echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';

										// Day display
										echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';

									} else if ( is_month() ) {

										// Month Archive

										// Year link
										echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
										// echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

										// Month display
										echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';

									} else if ( is_year() ) {

										// Display year archive
										echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';

									} else if ( is_author() ) {

										// Auhor archive

										// Get the author information
										global $author;
										$userdata = get_userdata( $author );

										// Display author name
										echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';

									} else if ( get_query_var('paged') ) {

										// Paginated archives
										echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';

									} else if ( is_search() ) {

										// Search results page
										echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';

									} elseif ( is_404() ) {

										// 404 page
										echo '<li>' . 'Error 404' . '</li>';
									}

									echo '</ol>';

								}

							}
