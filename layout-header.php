<?php
  global $post;
  if ( is_home([$post->ID]) || is_front_page([$post->ID]) ) {
    $class = 'text-center';
  }else{
    $class = 'text-left';
  }
?>
<header id="header" class="header <?php echo $class; ?>">
    <div class="container">
        <div class="branding">
          <?php $title = explode(' ', get_bloginfo('blog_name')); ?>
          <h1 class="logo">
              <a href="/">
                  <!-- <span aria-hidden="true" class="icon_documents_alt icon"></span> -->
                  <img src="<?php echo bloginfo('stylesheet_directory'); ?>/assets/images/logo.png" alt="">
                  <span class="text-highlight"><?php echo $title[0] ?></span> <span class="text-bold"><?php array_shift($title); echo join($title, ' '); ?></span>
              </a>
          </h1>
        </div><!--//branding-->
        <!-- <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li class="active">Charts</li>
        </ol> -->
        <?php custom_breadcrumbs(); ?>
    </div><!--//container-->
</header><!--//header-->
