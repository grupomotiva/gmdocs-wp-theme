<footer class="footer text-center">
    <div class="container">

        <small class="copyright">Desenvolvido com <i class="fa fa-heart-o" title="amor"></i> por <a href="http://agenciamav.com.br/?ref=docs.grupomotiva.com.br" target="_blank">Agência MAV</a> para o Grupo Motiva</small>

    </div><!--//container-->
</footer><!--//footer-->

<?php wp_footer(); ?>

</body>
</html>
