<?php get_header(); ?>

<body class="landing-page">

    <!--FACEBOOK LIKE BUTTON JAVASCRIPT SDK-->
    <div id="fb-root"></div>
    <!-- <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script> -->

    <div class="page-wrapper">

      <?php get_template_part('layout', 'header'); ?>

        <section class="cards-section">
            <div class="container">
                <h2 class="title text-center">Getting started is easy!</h2>

                <div class="intro">
                  <?php if ( have_posts() ) : ?>

                    <?php if ( is_home() && ! is_front_page() ) : ?>
                      <header>
                        <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                      </header>
                    <?php endif; ?>

                    <div id="cards-wrapper" class="cards-wrapper text-center row">

                      <?php echo do_shortcode('[gmdocs-childpages]'); ?>

                    </div><!--//cards-->

                    <?php
                    // Start the loop.
                    while ( have_posts() ) : the_post();

                      /*
                       * Include the Post-Format-specific template for the content.
                       * If you want to override this in a child theme, then include a file
                       * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                       */
                      get_template_part( 'content', get_post_format() );

                    // End the loop.
                    endwhile;

                    // Previous/next page navigation.
                    the_posts_pagination( array(
                      'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
                      'next_text'          => __( 'Next page', 'twentyfifteen' ),
                      'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                    ) );

                  // If no content, include the "No posts found" template.
                  else :
                    get_template_part( 'content', 'none' );

                  endif;
                  ?>
                    <!-- <div class="cta-container">
                        <a class="btn btn-primary btn-cta" href="http://themes.3rdwavemedia.com/" target="_blank"><i class="fa fa-cloud-download"></i> Download Now</a>
                    </div> -->
                </div><!--//intro-->

            </div><!--//container-->
        </section><!--//cards-section-->
    </div><!--//page-wrapper-->

<?php get_footer(); ?>
