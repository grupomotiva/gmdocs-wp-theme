<?php get_header(); ?>

<body class="body-green">
  <div class="page-wrapper">

    <?php get_template_part('layout', 'header'); ?>

    <div class="doc-wrapper">
      <div class="container">
        <div id="doc-header" class="doc-header text-center">
          <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> <?php echo the_title(); ?></h1>

          <div class="meta"><i class="fa fa-clock-o"></i> <strong>Atualizado em:</strong> <?php echo date_i18n('d \d\e M \d\e Y, \à\s H:i', strtotime($post->post_modified)); ?></div>
        </div><!--//doc-header-->




        <div class="doc-body">
          <div class="doc-content">
            <div class="content-inner">

              <div class="cards-section">
                <div class="intro">
                  <div id="cards-wrapper" class="cards-wrapper text-center row">

                    <?php echo do_shortcode('[gmdocs-childpages]'); ?>

                  </div>
                </div>
              </div>
              
              <?php if ( have_posts() ) : ?>

                <?php if ( is_home() && ! is_front_page() ) : ?>
                  <header>
                    <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                  </header>
                <?php endif; ?>

                <?php
                // Start the loop.
                while ( have_posts() ) : the_post();

                  /*
                  * Include the Post-Format-specific template for the content.
                  * If you want to override this in a child theme, then include a file
                  * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                  */
                  get_template_part( 'content', get_post_format() );

                  // End the loop.
                endwhile;

                  // Previous/next page navigation.
                  the_posts_pagination( array(
                    'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
                    'next_text'          => __( 'Next page', 'twentyfifteen' ),
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                  ) );

                  // If no content, include the "No posts found" template.
                else :
                  get_template_part( 'content', 'none' );

                endif;
                ?>

              </div><!--//content-inner-->
            </div><!--//doc-content-->
            <div class="doc-sidebar hidden-xs">
              <nav id="doc-nav">


                <ul id="doc-menu" class="nav doc-menu" data-spy="affix" data-offset-top="-80" v-html="sidemenu">

                  <!-- <li v-for="link in sidemenu" v-if="submenu" v-html="link"></li> -->
                  <!-- <li>
                    <a class="scrollto" href="#installation-section">Installation</a>
                    <ul class="nav doc-sub-menu">
                      <li v-for="link in submenu" v-if="submenu" v-html="link"></li>
                    </ul>
                  </li> -->

                </ul>


                </nav>
              </div><!--//doc-sidebar-->
            </div><!--//doc-body-->
          </div><!--//container-->
        </div><!--//doc-wrapper-->

        <?php get_template_part('layout', 'promoblock'); ?>

      </div><!--//page-wrapper-->

      <?php get_footer(); ?>
